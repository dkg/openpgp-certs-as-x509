Transporting OpenPGP Certificates as X.509 Certificates
=======================================================

This repository hosts the source for [draft-dkg-openpgp-certs-as-x509](https://datatracker.ietf.org/doc/draft-dkg-openpgp-certs-as-x509/).

You might want to read [the editor's copy](https://dkg.gitlab.io/openpgp-certs-as-x509/).
