---
title: Transporting OpenPGP Certificates as X.509 Certificates
abbrev: OpenPGP Certs as X.509
docname: draft-dkg-openpgp-certs-as-x509-00
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft

stand_alone: yes
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/dkg/openpgp-certs-as-x509"
  latest: "https://dkg.gitlab.io/openpgp-certs-as-x509/"

author:
  ins: D. K. Gillmor
  name: Daniel Kahn Gillmor
  org: American Civil Liberties Union
  street: 125 Broad St.
  city: New York, NY
  code: 10004
  country: USA
  abbrev: ACLU
  email: dkg@fifthhorseman.net
  role: editor
informative:
normative:
--- abstract

Some Internet protocols identify a participant with a cryptographic certificate, an object that binds identity information to public key material.
There are two well-defined and widely-implemented models of cryptographic certificate, with subtly different properties: X.509 certificates and OpenPGP certificates.

The two largest differences between these certificate models are:

- an X.509 certificate typically has a single issuer, while an OpenPGP certificate can have multiple, corroborative issuers
- an X.509 certificate typically associates a single asymmetric public key with the certificate subject, while an OpenPGP certificate allows the subject to add additional public keys as "subkeys".

In some of the circumstances where an X.509 certificate is expected, a relying party might prefer to do corroborative verification, or to see a range of subkeys.
To facilitate this, it can be useful for the party identifying itself to offer an OpenPGP certificate, or part of one, in place of or in addition to an X.509 certificate.

In other circumstances, any form of certificate will logically work, but only a mechanism for transporting X.509 is well-defined.

This document offers an interoperable mechanism for transporting an OpenPGP certificate within an X.509 certificate, extracting the OpenPGP certificate from an X.509 certificate, and describes specific scenarios where this can be useful.

--- middle

# Introduction

Both {{!OpenPGP=RFC4880}} and {{!X.509=RFC5280}} define wire formats that tie identity information to some asymmetric public key material.
This type of object is a cryptographic certificate, or just "Certificate" for short.

There are many Internet protocols that can make use of {{X.509}} certificates, but in some circumstances it would be advantageous to use an {{OpenPGP}} certificate instead.

This draft identifies different variations, including:

- how to populate the X.509 certificate subject public key information ({{spki}}),
- how to populate X.509 certificate subject identity ({{identity-data}}),
- how to populate certificate metadata ({{cert-metadata}}), and
- how to issue such an X.509 certificate ({{issuers}}).

Of these possible variants, this document identifies a small number of profiles that are concretely useful, the circumstances where they might be used, and justifications for why the specific techniques are selected.

## From OpenPGP To X.509 And Back

This document describes a way to transport an {{OpenPGP}} certificate inside an {{X.509}} certificate in such a way that the relying party is able to reconstruct the original OpenPGP certificate.
This permits any relying parties to use corroborative certificate verification from OpenPGP authorities.
Additionally, it enables a relying party to consider other public key material for the subscriber than the single public key found in most X.509 certificates. 

## OpenPGP From Existing X.509? 

FIXME: do we also want to define a mechanism for converting a generic X.509 certificate to OpenPGP?
What would that look like?
How/where would it be used?
How does the X.509 subscriber take advantage of it?
What does the relying party do?

# Terminology

OpenPGP Certificate

: This draft uses the term "OpenPGP Certificate" to refer to what {{OpenPGP}} calls a "Transferable public key".

X.509 Certificate

: This draft uses the term "X.509 Certificate" to refers to the `Certificate` object described in {{X.509}}.

##  Requirements Language

{::boilerplate bcp14-tagged}

# Use Cases

## OpenPGP Smart Card "Cardholder Certificate"

See https://gitlab.com/hkos/openpgp-card/-/issues/2

## TLS authentication

## OpenSSH X.509 certs

## S/MIME

## CMS Code Signing

## Anywhere Else X.509 is Used

# Emebdding OpenPGP Packets in X.509

An OpenPGP certificate is a series of OpenPGP packets.

This document defines an X.509 v3 extension whose contents is a binary-formatted sequence of OpenPGP packets.

## OpenPGPPackets X.509 v3 Extension {#x509v3-extension}

TODO: select OID

This extension contains a series of OpenPGP packets.
The series might be a full OpenPGP certificate, or it might be a selection of the packets involved.

See discussion (TBD: which section to point to?) about which packets might be included in different contexts.

## OpenPGP Public Key With External Key Material {#openpgp-contextual-key}

In some X.509 certificates, there is already public key material present in the Subject Public Key Information field.

For a public key algorithm with large keys, duplicating the same public key material in two places bloats the certificate.

When an OpenPGP Public Key Packet (or Public Subkey Packet) is embedded in a certificate where its key material is already present in the SPKI, a more compact certificate results from omitting the second copy of the key material.

TODO: describe a compact form of Public Key Packet, with algorithm-specific MPIs zeroed out.
Describe how to populate those values from a SPKI, rewriting the OpenPGP packet.
This probably means some algorithm-specific tables.

# Choosing the SPKI {#spki}

When setting up the Subject Public Key Identifier (SPKI) for the X.509 certificate, there are three main choices:

- SPKI refers to the OpenPGP key by containing its fingerprint ({{spki-fingerprint}})
- SPKI contains the primary key of the OpenPGP certificate ({{spki-primary}})
- SPKI contains a subkey of the OpenPGP certificate ({{spki-subkey}})

## SPKI is OpenPGP Fingerprint {#spki-fingerprint}

This document defines an AlgorithmIdentifier for the SubjectPublicKeyInfo.algorithm field that stores only the OpenPGP fingerprint of the primary key in question.

The OpenPGP certificate is included in standard form as an X.509 extension as described in {{x509v3-extension}}.

TODO: select OID

TODO: might you ever want the "subject" to be the fingerprint of a subkey instead of the primary key?
If not, why not?

## SPKI is Primary Key {#spki-primary}

To form this type of X.509 certificate:

- Identify the primary key's public key algorithm, and map it to the SPKI algorithm and parameters as found in {{pubkeyid-algoid}}.
- Extract the public key material from the MPIs and transform it into the expected ASN.1 form.
- Convert the OpenPGP public key packet containing the primary key to the form described in {{openpgp-contextual-key}}, and include it (and any other desired OpenPGP packets from the certificate) as an extension as described in {{x509v3-extension}}.

{: title="OpenPGP Public Key algorithm to SPKI algorithmIdentifier" #pubkeyid-algoid}
| OpenPGP version | OpenPGP Pubkey Algorithm ID | OpenPGP ECC Curve OID | SPKI algorithmIdentifier algorithm OID | SPKI algorithmIdentifier parameters |
|=====|======|=====|====|====|
| V4 | 1 (RSA) | N/A | 1.2.840.113549.1.1.1 (rsaEncryption??) | NULL |
| V4 | 16 (Elgamal) | N/A | TODO | TODO |
| V4 | 17 (DSA) | N/A | TODO | TODO |
| V4 | 18 (ECDH) | TODO: NIST curves  | 1.2.840.10045.2.1 (ecPublicKey) | TODO |
| V4 | 18 (ECDH) | 1.3.6.1.4.1.3209.15.1 (Curve25519) | 1.3.101.110 (curveX25519) | N/A |
| V4 | 18 (ECDH) | 1.3.101.113 (Ed448) | 1.3.101.113 (curveEd448) | N/A |
| V4 | 19 (ECDSA) | TODO: NIST curves | 1.2.840.10045.2.1 (ecPublicKey) | TODO |
| V4 | 22 (EdDSA) | 1.3.6.1.4.1.11591.15.1 (Ed25519) | 1.3.101.112 (curveEd25519) | N/A |
| V4 | 22 (EdDSA) | 1.3.101.113 (Ed448) | 1.3.101.113 (curveEd448) | N/A |

## SPKI is Subkey {#spki-subkey}

In some cases, the X.509 certificate needs to foreground a specific subkey.

For example, an OpenPGP certificate might have a subkey that is dedicated for code signing in a context that uses CMS object signatures.

The X.509 certificate for making those CMS signatures needs the subject public key to be the subkey in particular, because that is the key material used to verify the software before running.

To form this type of X.509 certificate:

- Identify the relevant subkey in the OpenPGP certificate.
- Map the subkey's public key algorithm to the SPKI algorithm and parameters as found in {{pubkeyid-algoid}}.
- Extract the public key material from the subkey's MPIs and transform it into the expected ASN.1 form.
- Convert the OpenPGP public subkey packet containing the primary key to the form described in {{openpgp-contextual-key}}.
- Assemble an OpenPGP certificate including at least the associated primary key, the relevant subkey, the subkey binding signature (including any applicable cross-signature), and include the assembled certificate as an extension as described in {{x509v3-extension}}.

# Populating X.509 Identity Data {#identity-data}

Regardless of what gets placed in the SPKI (as described in {{spki}}), the resultant X.509 certificate needs to contain some identity data.

The most common forms of this identity data are the Subject field, and the subjectAltName extension.

## Subject

Use of the Subject field in X.509 during validation is generally discouraged.

For any X.509 certificate that wraps an OpenPGP certificate, the Subject DN should be a single element of type OpenPGP Fingerprint, with the value set to the fingerprint of the primary key.

TODO: define OID for this type.

## subjectAltName

TODO: define how to map OpenPGP User ID (or other information?) into a coherent subjectAltName.


# Populating X.509 Certificate Metadata {#cert-metadata}

In addition to identity information, an X.509 certificate contains additional metadata about when, where, and how the given cryptographic material may be used in association with the corresponding identity.

## Validity Window

TODO: how to populate this?

## Key Usage

TODO: map from OpenPGP key usage flags to x.509 keyUsage and extendedKeyUsage

TODO: when an OpenPGP certificate contains multiple keys, should the X.509 KU and EKU correspond to the union of the valid key flags in the entire OpenPGP certificate, or to some subset?
Is it different if it is a subkey?

## Serial Number

TODO: what serial number should be put in the X.509 certificate?
Should it be deterministic in any case?

# Reconstructing the OpenPGP Certificate

TODO: from an X.509 certificate, or a set of X.509 certificates, how does the relying party reconstruct the full OpenPGP certificate?

TODO: reconsituting OpenPGP pubkey data from SPKI

TODO: if X.509 metadata does not match OpenPGP key data, reject cert or ignore X.509?

# Options for Certificate Issuer {#issuers}

There are three distinct ways to structure the resultant X.509 certificate's issuer:

- Issue the certificate from a typical Certification Authority ({{issue-ca}})
- Issue the certificate, creating a signature from from the OpenPGP primary key itself ({{issue-self-signed}})
- Avoid the issuance process, using a boilerplate, non-cryptographic pseudosignature ({{issue-unsigned}})

## Issuing from a Certification Authority {#issue-ca}

TODO: if a public Certification Authority wants to accept a CSR with such an extension, how should it confirm and align the various fields?

TODO: can a public Certification Authority treat a self-signed OpenPGP certificate as a CSR?
What should it generate?

TODO: If the authority wants to also include their own OpenPGP third-party certification, how should it do that?

### Reissuance Cadence {#ca-reissuance-cadence}

When should the certificate be re-issued?

### Revocation, OCSP Stapling

## Self-signed Certificate {#issue-self-signed}

In some circumstances, the subject may not have access to an external Certification Authority willing to issue a certificate with the OpenPGPPackets X.509 v3 extension.
In other circumstances, a CA's signature may not be useful, and space constraints encourage a more compact wire representation.

In either of these circumstances, the subject will produce an X.509 certificate is effectively self-signed.

There are two different ways to produce a self-signed X.509 certificate that incorporates an OpenPGP certificate.

TODO: Make a self-signed cert over the OpenPGP key or subkey in question using the OpenPGP primary key.

TODO: need table mapping OpenPGP public key types and hash algorithms to standard X.509 SignatureAlgorithm OIDs.

### Reissuance Cadence {#self-reissuance-cadence}

### Issuer Copies Subject {#selfsigned-same-issuer}

This certificate is likely to be more interoperable (the relying party won't choke on an unknown SignatureAlgorithm) but it is larger by the size of the signature.

## Unsigned Certificates {#issue-unsigned}

TODO: describe ASN.1 wrapping with no actual signature data.

This is the most compact approach, but may be rejected by some systems which don't recognize the custom signatureAlgorithm.

### Issuer Copies Subject {#unsigned-same-issuer}

### "Only OpenPGP" SignatureAlgorithm

TODO: declare an OID for signatureAlgorithm that means "ignore this signature, rely on the embedded OpenPGP certifications instead".
This algorithm will have a zero-length signature BIT STRING.

### Reissuance Cadence {#unsigned-reissuance-cadence}

This certificate never needs to be re-issued as long as the OpenPGP material has not changed.

When the OpenPGP material has changed, a new X.509 certificate or certificates MUST be generated, replacing the older ones.

### Third-party Generation

Anyone can transform an OpenPGP certificate to an unsigned X.509 certificate in this way -- no secret key or special authority is needed.


# Specific Profiles

Of the various options described in this draft, only some combinations of them have plausible use.
This section describes a limited number of profiles.

## Simple Wrapper

The Simple Wrapper profile simply permits an OpenPGP certificate to be transferred across a channel designed for X.509, between parties that know how to handle an OpenPGP certificate.

- no issuer
- OpenPGP Fingerprint SPKI
- minimal identity data and metadata

### Using a Simple Wrapper in S/MIME (CMS E-mail)

TODO: can we define a new form of S/MIME signature object that embeds an OpenPGP signature, which can then verify the signed message?

TODO: can we define a new form of S/MIME envelopedData (or something similar) that just contains an OpenPGP encryted message?

## Code Signing

TODO: identify some contexts in which a code-signing cert might stem from an OpenPGP cert, describe who should validate, etc.

## Network Authentication

TODO: work through TLS/SSH server/client authentication

# Guidance for Relying Parties

In different certificate contexts, a relying party that wants to do corroborative verification might care about specific details in a certificate and not others.
The relying party might also want to indicate to the other party that such a certificate is desirable.

## Corroborative Verification

FIXME: describe some common use contexts and how an OpenPGP certificate verification could add corroboration to an X.509 certificate.

### Corroborating the Identity of an E-mail Correspondent

### Corroborating the Identity of an HTTPS Server

### Corroborating the Identity of a Software Developer

## Signalling Support and Preferences

TODO: context-specific ways to indicate to a peer that a certificate with OpenPGP corroborative data is useful/accepted/preferred.

# Security Considerations

## Avoid Cross-protocol Attacks

TODO: secret key material that might get used in multiple contexts -- is there an issue if it gets used in some standard X.509 way?

# Privacy Considerations

## Network Activity

### Revocation

### Keyserver Lookups

#### HKP and HKPS

#### SKS

### WKD

### DANE OPENPGPKEY

## Certificate Leakage In Older TLS Handshakes

## Padding For Metadata Resilience

# IANA Considerations

--- back

# Test Vectors

## Unsigned Certificates

TODO: convert Alice and Bob's certs from {{?I-D.bre-openpgp-samples}} using "Only OpenPGP" signatureAlgorithm

## Self-signed Certificates

TODO: convert Alice and Bob's certs from {{?I-D.bre-openpgp-samples}} as self-signed signatures.

## From a Certification Authority

TODO: convert Alice and Bob's certs from {{?I-D.bre-openpgp-samples}} using the authority in {{?I-D.ietf-lamps-samples}}

# Sample Code

# Document History
